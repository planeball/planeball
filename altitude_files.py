import os


class AltitudeFiles:
    def __init__(self, launcher_config: str):
        self.directory_py = str(os.path.dirname(__file__))
        self.directory_servers = str(os.path.dirname(__file__).rsplit('/', 1)[0]) + '/'
        self.server_log = os.path.join(self.directory_servers, 'log.txt')
        self.server_log_old = os.path.join(self.directory_servers, 'log_old.txt')
        self.server_log_new = os.path.join(self.directory_servers, 'log_old_')
        self.command = os.path.join(self.directory_servers, 'command.txt')
        self.launcher_config = os.path.join(self.directory_servers, launcher_config + '.xml')
        self.ban_list = os.path.join(self.directory_servers, 'ban_list.xml')

    def files_exist(self):
        if not os.path.exists(self.server_log):
            return False, 'Required file servers/log.txt not found'
        elif not os.path.exists(self.command):
            return False, 'Required file servers/command.txt not found'
        elif not os.path.exists(self.launcher_config):
            return False, 'Required file ' + self.launcher_config + ' not found'
        return True, "All Files Exist"
