import logging
from collections import OrderedDict
from planeball_map import AltitudeMap
try:
    import xml.etree.cElementTree as ElementTree
except ImportError:
    import xml.etree.ElementTree as ElementTree


class ConfigBots:
    def __init__(self, number_of_bots: int, bots_balance_teams: bool, bot_spectate_threshold: int):
        self.number_of_bots = number_of_bots
        self.bots_balance_teams = bots_balance_teams
        self.bot_spectate_threshold = bot_spectate_threshold


class ConfigFFA:
    def __init__(self, round_limit: int, round_time_seconds: int, warmup_time_seconds: int, score_limit: int):
        self.round_limit = round_limit
        self.round_time_seconds = round_time_seconds
        self.warmup_time_seconds = warmup_time_seconds
        self.score_limit = score_limit


class ConfigTBD:
    def __init__(self, round_limit: int, round_time_seconds: int, warmup_time_seconds: int):
        self.round_limit = round_limit
        self.round_time_seconds = round_time_seconds
        self.warmup_time_seconds = warmup_time_seconds


class ConfigObjective:
    def __init__(self, round_limit: int, round_time_seconds: int, warmup_time_seconds: int, games_per_round: int,
                 games_per_switch_sides: int, game_win_margin: int, between_game_time_seconds: int):
        self.round_limit = round_limit
        self.round_time_seconds = round_time_seconds
        self.warmup_time_seconds = warmup_time_seconds
        self.games_per_round = games_per_round
        self.games_per_switch_sides = games_per_switch_sides
        self.game_win_margin = game_win_margin
        self.between_game_time_seconds = between_game_time_seconds
        games_per_round_even = games_per_round
        if games_per_round % 2 == 1:
            games_per_round_even += 1
        self.games_to_win = games_per_round_even / 2


class ConfigBALL:
    def __init__(self, round_limit: int, round_time_seconds: int, warmup_time_seconds: int, goals_per_round: int):
        self.round_limit = round_limit
        self.round_time_seconds = round_time_seconds
        self.warmup_time_seconds = warmup_time_seconds
        self.goals_per_round = goals_per_round


class ConfigTDM:
    def __init__(self, round_limit: int, round_time_seconds: int, warmup_time_seconds: int, score_limit: int):
        self.round_limit = round_limit
        self.round_time_seconds = round_time_seconds
        self.warmup_time_seconds = warmup_time_seconds
        self.score_limit = score_limit


class ServerConfig:
    def __init__(self, port: int, server_name: str, short_name: str, max_player_count: int, hardcore: bool,
                 auto_balance_teams: bool, prevent_team_switching: bool, disable_balance_teams_popup: bool,
                 max_ping: int, secret_code: str, camera_view_scale_percent: int, bot_config: ConfigBots,
                 ffa_game_mode: ConfigFFA, tbd_game_mode: ConfigTBD, objective_game_mode: ConfigObjective,
                 ball_game_mode: ConfigBALL, tdm_game_mode: ConfigTDM, map_rotation_list: list, map_list:  dict,
                 maps_by_mode: dict, maps_by_group: dict, lobby: str):
        self.port = port
        self.server_name = server_name
        self.short_name = short_name
        self.max_player_count = max_player_count
        self.hardcore = hardcore
        self.auto_balance_teams = auto_balance_teams
        self.prevent_team_switching = prevent_team_switching
        self.disable_balance_teams_popup = disable_balance_teams_popup
        self.max_ping = max_ping
        self.secret_code = secret_code
        self.camera_view_scale_percent = camera_view_scale_percent
        self.bot_config = bot_config
        """ :type: ConfigBots """
        self.ffa_game_mode = ffa_game_mode
        """ :type: ConfigFFA """
        self.tbd_game_mode = tbd_game_mode
        """ :type: ConfigTBD """
        self.objective_game_mode = objective_game_mode
        """ :type: ConfigObjective """
        self.ball_game_mode = ball_game_mode
        """ :type: ConfigBall """
        self.tdm_game_mode = tdm_game_mode
        """ :type: ConfigTDM """
        self.map_rotation_list = map_rotation_list
        """ :type: list [str] """
        self.map_list = map_list
        """ :type: dict [str, AltitudeMap] """
        self.maps_by_mode = maps_by_mode
        """ :type: dict [str, list [str]] """
        self.maps_by_group = maps_by_group
        """ :type: dict [str, list [str]] """
        self.lobby = lobby
        """ :type: str """


def read_launcher_config(launcher_config: str):
    config_servers = OrderedDict()
    tree = ElementTree.parse(launcher_config)
    root = tree.getroot()
    for child in root[0]:
        lobby = None
        map_rotation_list = list()
        map_list = dict()
        maps_by_mode = dict()
        """ :type: dict [str, list [str]] """
        maps_by_group = dict()
        """ :type: dict [str, list [str]] """
        config_bots = None
        config_ball = None
        config_tbd = None
        config_tdm = None
        config_objective = None
        config_ffa = None
        for sub_child in child:
            if sub_child.tag == 'mapRotationList':
                for map_name in sub_child:
                    if map_name.attrib['value'].upper().startswith('LOBBY'):
                        if lobby is None:
                            lobby = map_name
                        else:
                            logging.exception('Each server can only include one lobby.')
                            raise Exception('Each server can only include one lobby.')
                    map_rotation_list.append(map_name.attrib['value'].lower())
                if lobby is not None and len(launcher_config) > 1:
                    logging.exception('If a server has a lobby in the map rotation, '
                                      'it must be the only map in the rotation.')
                    raise Exception('If a server has a lobby in the map rotation, '
                                    'it must be the only map in the rotation.')
            elif sub_child.tag == 'mapList':
                for map_name in sub_child:
                    altitude_map = AltitudeMap(map_name.attrib['value'])
                    map_list[altitude_map.name] = altitude_map
                    if altitude_map.mode == 'LOBBY':
                        if lobby is None:
                            lobby = map_name.attrib['value']
                        else:
                            logging.exception('Each server can only include one lobby.')
                            raise Exception('Each server can only include one lobby.')
                    else:
                        if altitude_map.mode not in maps_by_mode.keys():
                            maps_by_mode[altitude_map.mode] = list()
                            if altitude_map.get_rating_group() not in maps_by_mode.keys():
                                maps_by_group[altitude_map.get_rating_group()] = list()
                        maps_by_mode[altitude_map.mode].append(altitude_map.name)
                        maps_by_group[altitude_map.get_rating_group()].append(altitude_map.name)
            elif sub_child.tag == 'BotConfig':
                config_bots = ConfigBots(int(sub_child.attrib['numberOfBots']),
                                         bool(sub_child.attrib['botsBalanceTeams']),
                                         int(sub_child.attrib['botSpectateThreshold']))
            elif sub_child.tag == 'FreeForAllGameMode':
                config_ffa = ConfigFFA(int(sub_child.attrib['RoundLimit']), int(sub_child.attrib['roundTimeSeconds']),
                                       int(sub_child.attrib['warmupTimeSeconds']), int(sub_child.attrib['scoreLimit']))
            elif sub_child.tag == 'BaseDestroyGameMode':
                config_tbd = ConfigTBD(int(sub_child.attrib['RoundLimit']), int(sub_child.attrib['roundTimeSeconds']),
                                       int(sub_child.attrib['warmupTimeSeconds']))
            elif sub_child.tag == 'ObjectiveGameMode':
                config_objective = ConfigObjective(int(sub_child.attrib['RoundLimit']),
                                                   int(sub_child.attrib['roundTimeSeconds']),
                                                   int(sub_child.attrib['warmupTimeSeconds']),
                                                   int(sub_child.attrib['gamesPerRound']),
                                                   int(sub_child.attrib['gamesPerSwitchSides']),
                                                   int(sub_child.attrib['gameWinMargin']),
                                                   int(sub_child.attrib['betweenGameTimeSeconds']))
            elif sub_child.tag == 'PlaneBallGameMode':
                config_ball = ConfigBALL(int(sub_child.attrib['RoundLimit']),
                                         int(sub_child.attrib['roundTimeSeconds']),
                                         int(sub_child.attrib['warmupTimeSeconds']),
                                         int(sub_child.attrib['goalsPerRound']))
            elif sub_child.tag == 'TeamDeathmatchGameMode':
                config_tdm = ConfigTDM(int(sub_child.attrib['RoundLimit']), int(sub_child.attrib['roundTimeSeconds']),
                                       int(sub_child.attrib['warmupTimeSeconds']), int(sub_child.attrib['scoreLimit']))
        if len(map_list) == 0:
            logging.exception('Map list cannot be empty.')
            raise Exception('Map list cannot be empty.')
        if len(map_rotation_list) == 0:
            logging.exception('Map rotation list cannot be empty.')
            raise Exception('Map rotation list cannot be empty.')
        if config_bots is None:
            logging.exception('Bot config is missing.')
            raise Exception('Bot config is missing.')
        if config_ball is None:
            logging.exception('Ball config is missing.')
            raise Exception('Ball config is missing.')
        if config_tbd is None:
            logging.exception('TBD config is missing.')
            raise Exception('TBD config is missing.')
        if config_tdm is None:
            logging.exception('TDM config is missing.')
            raise Exception('TDM config is missing.')
        if config_objective is None:
            logging.exception('Objective config is missing.')
            raise Exception('Objective config is missing.')
        if config_ffa is None:
            logging.exception('FFA config is missing.')
            raise Exception('FFA config is missing.')
        port = int(child.attrib['port'])
        short_name = child.attrib['serverName']
        if 'shortName' in root.attrib:
            short_name = child.attrib['shortName']
        config_servers[port] = ServerConfig(port, child.attrib['serverName'], short_name,
                                            int(child.attrib['maxPlayerCount']), bool(child.attrib['hardcore']),
                                            bool(child.attrib['autoBalanceTeams']),
                                            bool(child.attrib['preventTeamSwitching']),
                                            bool(child.attrib['disableBalanceTeamsPopup']),
                                            int(child.attrib['maxPing']), child.attrib['secretCode'],
                                            int(child.attrib['cameraViewScalePercent']), config_bots, config_ffa,
                                            config_tbd, config_objective, config_ball, config_tdm, map_rotation_list,
                                            map_list, maps_by_mode, maps_by_group, lobby)
    return config_servers

