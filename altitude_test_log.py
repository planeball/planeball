from time import time
from random import randint
from planeball_plane import Plane


class LogPlayer:
    def __init__(self, vapor_id: str, nickname: str, level: int, ace_rank: int, ip: str):
        self.vapor_id = vapor_id
        self.nickname = nickname
        self.level = level
        self.ace_rank = ace_rank
        self.ip = ip
        self.number = -10
        self.team = -10
        self.plane = Plane('Loopy', 'Double Fire', 'Heavy Armor', 'Turbocharger', 'No Skin')

    def set_random_plane(self):
        planes = ['Loopy', 'Bomber', 'Biplane', 'Explodet', 'Miranda']
        perks_red_loopy = ['Tracker', 'Double Fire', 'Acid Bomb', ]
        perks_red_bomber = ['Flak Tailgun', 'Bombs', 'Suppressor', ]
        perks_red_biplane = ['Dogfighter', 'Heavy Cannon', 'Recoilless Gun']
        perks_red_explodet = ['Director', 'Thermobarics', 'Remote Mine']
        perks_red_miranda = ['Trickster', 'Laser', 'Time Anchor']
        perks_green = ['No Green Perk', 'Rubberized Hull', 'Heavy Armor', 'Repair Drone', 'Flexible Wings']
        perks_blue = ['No Blue Perk', 'Turbocharger', 'Ultracapacitor', 'Reverse Thrust', 'Ace Instincts']
        skins = ['No Skin', 'Checker Skin', 'Flame Skin', 'Santa Hat', 'Shark Skin', 'Zebra Skin']
        plane_type = planes[randint(0, len(planes) - 1)]
        perk_red = None
        if plane_type == 'Loopy':
            perk_red = perks_red_loopy[randint(0, len(perks_red_loopy) - 1)]
        elif plane_type == 'Bomber':
            perk_red = perks_red_bomber[randint(0, len(perks_red_bomber) - 1)]
        elif plane_type == 'Biplane':
            perk_red = perks_red_biplane[randint(0, len(perks_red_biplane) - 1)]
        elif plane_type == 'Explodet':
            perk_red = perks_red_explodet[randint(0, len(perks_red_explodet) - 1)]
        elif plane_type == 'Miranda':
            perk_red = perks_red_miranda[randint(0, len(perks_red_miranda) - 1)]
        perk_green = perks_green[randint(0, len(perks_green) - 1)]
        perk_blue = perks_green[randint(0, len(perks_blue) - 1)]
        skin = perks_green[randint(0, len(skins) - 1)]
        self.plane = Plane(plane_type, perk_red, perk_green, perk_blue, skin)


class LogWriter:
    def __init__(self, file_name: str, port_list):
        self.log_file = file_name
        self.ports = port_list
        self.player_count = 0

    def log(self, new_line: str):
        with open(self.log_file, "a") as log_file:
            log_file.write(new_line)
            log_file.close()

    def client_add(self, port: int, log_time: int, player: LogPlayer):
        player.number = self.player_count
        self.player_count += 1
        self.log('{"port":' + str(port) + ',"demo":false,"time":' + str(log_time) +
                 ',"level":' + str(player.level) + ',"player":' + str(self.player_count) +
                 ',"nickname":"' + player.nickname + '","aceRank":' + str(player.ace_rank) +
                 ',"vaporId":"' + player.vapor_id + '","type":"clientAdd","ip":"' + player.ip + '"}' + "\n")

    def client_remove(self, port: int, log_time: int, log_player: LogPlayer, reason: str):
        self.log('{"port":' + str(port) + ',"message":"left","time":' + str(log_time) +
                 ',"player":' + str(log_player.number) + ',"reason":"' + str(reason) +
                 '","nickname":"' + log_player.nickname + '","vaporId":"' + log_player.vapor_id +
                 ',"type":"clientRemove","ip":"' + log_player.ip)

    def team_change(self, port: int, log_time: int, player: LogPlayer, team: int):
        player.team = team
        self.log('{"port":' + str(port) + ',"time":' + str(log_time) + ',"player":' + str(player.number) +
                 ',"type":"teamChange"}')

    def spawn(self, port: int, log_time: int, plane: str, player: LogPlayer):
        self.log('{"port":' + str(port) + ',"time":' + str(log_time) + ',"plane":"' + plane + '","player":' +
                 str(player.number) + ',"perkRed":"' + str(player.plane.perk_red) +
                 '","perkGreen":"' + str(player.plane.perk_green) + '","team":' + str(player.team) +
                 ',"type":"spawn","perkBlue":"' + player.plane.perk_blue + '","skin":' + player.plane.skin + '"}')


def escape(text: str):
    return text.replace('\\', '\\\\').replace('"', '\\"')

