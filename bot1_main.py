import discord
from discord import app_commands
import asyncio
import logging
from collections import OrderedDict
from datetime import date, timedelta, datetime
from prettytable import PrettyTable
from planeball_seasons import Season, get_seasons
from planeball_database import DatabaseQueue
from bot1_commands import *
from bot1_config import DiscordConfig
from planeball_credentials import Credentials


discord_config = DiscordConfig('bot1_config.xml')
credentials = Credentials('planeball_credentials.xml', discord_config.log_file, discord_config.log_level)

db = DatabaseQueue(credentials.database, discord_config.log_file, discord_config.log_level)
seasons = OrderedDict()
""" :type: OrderedDict """
last_daily_update = None
""" :type: datetime.date """


class Bot1(discord.Client):
    def __init__(self):
        super().__init__(intents=intents)
        self.synced = False

    async def on_ready(self):
        await self.wait_until_ready()
        if not self.synced:
            await tree.sync(guild=discord.Object(id=discord_config.id_guild))
            self.synced = True
        logging.info(f'We have logged in as {self.user}')

    async def bot_logic(self):
        logging.debug('Start Bot Logic')
        try:
            while True:
                logging.debug('***** BOT LOOP: Start ****')
                global last_daily_update, seasons
                if last_daily_update is None or last_daily_update < date.today():
                    if last_daily_update is None:
                        logging.info('Executing First Run Daily Maintenance Tasks')
                    else:
                        logging.info('Executing Daily Maintenance Tasks')
                    last_daily_update = date.today()
                    seasons = get_seasons(db, 'Ranked')
                    logging.info('Seasons Loaded: ' + str(seasons))
                logging.debug('***** BOT LOOP: End ****')
                await asyncio.sleep(10)
        except Exception as e:
            logging.exception(e)
        finally:
            logging.critical('Bot logic loop ended unexpectedly')


intents = discord.Intents.default()
intents.message_content = True

client = Bot1()
""" :type: Bot1 """
tree = app_commands.CommandTree(client)


@tree.command(name="id", description="Get player info using ID", guild=discord.Object(id=discord_config.id_guild))
async def self(interaction: discord.Interaction, id_player:int):
    logging.info('/id ' + str(id_player) + ' called by ' + interaction.user.name)
    await interaction.response.send_message(slash_id(id_player, db, seasons), ephemeral=True)


@tree.command(name="aka", description="Get player info using @aka", guild=discord.Object(id=discord_config.id_guild))
async def self(interaction: discord.Interaction, aka:str):
    logging.info('/aka ' + aka + ' called by ' + interaction.user.name)
    await interaction.response.send_message(slash_aka(aka, db, seasons), ephemeral=True)


async def main():
    async with client:
        client.loop.create_task(client.bot_logic())
        await client.start(discord_config.token)


asyncio.run(main())
