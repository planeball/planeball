from xml.etree import ElementTree
import logging


class Credentials:
    def __init__(self, file: str, log_file: str, log_level: int):
        self.database = {}
        self.twitter_access_token = None
        self.twitter_access_secret = None
        self.twitter_consumer_key = None
        self.twitter_consumer_secret = None
        self.email_host = None
        self.email_port = None
        self.email_from = None
        self.email_to = None
        self.email_pass = None
        self.read_xml_credentials(file)

    def read_xml_credentials(self, file: str):
        file_exists = True
        try:
            tree = ElementTree.parse(file)
            root = tree.getroot()
            email_settings = None
            database_settings = None
            twitter_settings = None
            for child in root:
                if child.tag.upper() == "DATABASE":
                    for child_database in child:
                        if child_database.tag.upper() == 'USER':
                            self.database['user'] = child_database.attrib['value']
                        elif child_database.tag.upper() == 'PASSWORD':
                            self.database['password'] = child_database.attrib['value']
                        elif child_database.tag.upper() == 'HOST':
                            self.database['host'] = child_database.attrib['value']
                        elif child_database.tag.upper() == 'DATABASE':
                            self.database['database'] = child_database.attrib['value']
                elif child.tag.upper() == "TWITTER":
                    twitter_settings = {'access_token': '', 'access_secret': '', 'consumer_key': '', 'consumer_secret': ''}
                    for child_twitter in child:
                        if child_twitter.tag.upper() == 'ACCESS_TOKEN':
                            twitter_settings['access_token'] = child_twitter.attrib['value']
                        elif child_twitter.tag.upper() == 'ACCESS_SECRET':
                            twitter_settings['access_secret'] = child_twitter.attrib['value']
                        elif child_twitter.tag.upper() == 'CONSUMER_KEY':
                            twitter_settings['consumer_key'] = child_twitter.attrib['value']
                        elif child_twitter.tag.upper() == 'CONSUMER_SECRET':
                            twitter_settings['consumer_secret'] = child_twitter.attrib['value']
                elif child.tag.upper() == "EMAIL":
                    email_settings = {'host': '', 'port': '', 'from:': '', 'to': '', 'pass': ''}
                    for child_email in child:
                        if child_email.tag.upper() == 'EMAIL_HOST':
                            email_settings['host'] = child_email.attrib['value']
                        elif child_email.tag.upper() == 'EMAIL_PORT':
                            email_settings['port'] = int(child_email.attrib['value'])
                        elif child_email.tag.upper() == 'EMAIL_FROM':
                            email_settings['from'] = child_email.attrib['value']
                        elif child_email.tag.upper() == 'EMAIL_PASS':
                            email_settings['pass'] = child_email.attrib['value']
                        elif child_email.tag.upper() == 'EMAIL_TO':
                            email_settings['to'] = child_email.attrib['value']
        except FileNotFoundError:
            logging.exception('Credentials XML file (' + str(file) + ') does not exist.')
            file_exists = False
        finally:
            if not file_exists:
                raise Exception('Credentials XML file (' + str(file) + ') does not exist.')
            elif 'host' not in self.database.keys():
                raise Exception('Database host not found in Credentials XML file (' + str(file) + ').')
            elif 'database' not in self.database.keys():
                raise Exception('Database name not found in Credentials XML file (' + str(file) + ').')
            elif 'user' not in self.database.keys():
                raise Exception('Database user not found in Credentials XML file (' + str(file) + ').')
            elif 'password' not in self.database.keys():
                raise Exception('Database user password not found in Credentials XML file (' + str(file) + ').')
            else:
                logging.info('Credentials loaded successfully')
