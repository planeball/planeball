class Plane:
    def __init__(self, plane: str, perk_red: str, perk_green: str, perk_blue: str, skin: str):
        self.plane_type = plane
        self.perk_red = perk_red
        self.perk_green = perk_green
        self.perk_blue = perk_blue
        self.skin = skin
