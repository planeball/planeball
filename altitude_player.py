from altitude_stats import *


class Player:
    def __init__(self, player_number: int, vapor_id: str, nickname: str, ip: str, level: int, ace_rank: int,
                 joined_game_time: int):
        self.player_number = player_number
        self.vapor_id = vapor_id
        if nickname.startswith('Bot '):
            split_nickname = nickname.split(' ', 1)
            if len(split_nickname) == 2:
                bot_number = split_nickname[1]
                if bot_number.isdigit():
                    self.vapor_id = str(-1000 - int(bot_number))
        self.nickname = nickname
        self.last_three_nicknames = None
        """ :type: list [str] """
        self.ip = ip
        self.level = level
        self.ace_rank = ace_rank
        self.aka = None
        self.is_admin = False
        self.is_lead_admin = False
        self.is_developer = False
        self.is_league_admin = False
        self.ball_clan = None
        self.tbd_clan = None
        self.ball_clan_capt = None
        self.tbd_clan_capt = None
        self.joined_game_time = joined_game_time
        self.initialized = False
        self.account_id = None
        self.country_code = None
        self.continent = None
        self.region = None
        self.target_player_id = None
        self.mode_stats = None
        """ :type: dict [str, dict [str, ModeSeasonStats]] """
        self.group_stats = None
        """ :type: dict [str, dict [str, RatingGroupSeasonStats]] """
        self.group_tiers = dict()
        """ :type: dict [str, dict [str, int]] """
        self.consecutive_days = 0
        self.last_played = None
        """ :type: datetime.datetime """
        self.game_data = TournamentData()
        """ :type: TournamentData """
        self.disconnect_time = None
        self.rating_exception = False
        self.hide_rating_ball = False

    def add_group_stat(self, league: str, rating_group: str):
        self.group_stats[league][rating_group] = RatingGroupSeasonStats()

    def add_mode_stat(self, league: str, mode_name: str):
        self.mode_stats[league][mode_name] = ModeSeasonStats()

    def reset_game(self):
        self.game_data = TournamentData()

    def spawn(self, vapor_id: str, red_perk: str, green_perk: str, blue_perk: str, team_number: int, log_time: int):
        self.game_data.spawn_stats_active = PlaneStats(vapor_id, red_perk, green_perk, blue_perk, team_number)
        self.game_data.spawn_stats_active.spawn_start = log_time

    def nickname_change(self, new_nickname):
        if self.initialized and self.last_three_nicknames is not None:
            if new_nickname in self.last_three_nicknames:
                self.last_three_nicknames.remove(new_nickname)
            elif len(self.last_three_nicknames) == 3:
                self.last_three_nicknames.pop(2)
            self.last_three_nicknames.append(self.nickname)
        self.nickname = new_nickname

    def get_rating(self, game_mode):
        if self.group_stats is not None:
            if game_mode in self.group_stats['Ranked']:
                return self.group_stats['Ranked'][game_mode].rating
            else:
                return None
