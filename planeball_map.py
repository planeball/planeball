def parse_map_name(map_name: str):
    if map_name.upper().startswith('BALL_') or map_name.upper().startswith('TBD_') or \
            map_name.upper().startswith('TDM_') or map_name.upper().startswith('1DM_') or \
            map_name.upper().startswith('1BD_') or map_name.upper().startswith('1DE_') or \
            map_name.upper().startswith('LOBBY_'):
        split_map_name = map_name.split('_', 1)
        map_prefix = split_map_name[0].upper()
        if map_name.upper().startswith('BALL_4'):
            map_mode = '4BALL'
        elif map_name.upper().startswith('TBD_4'):
            map_mode = '4TBD'
        else:
            map_mode = split_map_name[0].upper()
        short_map_name = split_map_name[1]
    else:
        map_mode = map_name
        map_prefix = ''
        short_map_name = map_name
    return map_mode, map_prefix, short_map_name


class AltitudeMap:
    def __init__(self, map_name: str):
        map_info = parse_map_name(map_name)
        self.mode = map_info[0]
        self.prefix = map_info[1]
        self.short_name = map_info[2]
        self.name = map_name

    def get_rating_group(self):
        # allows for multiple modes to be tied to the same rating group
        if self.mode == 'BALL':
            return 'BALL'
        elif self.mode == '4BALL':
            return '4BALL'
        elif self.mode == 'TBD':
            return 'TBD'
        elif self.mode == '4TBD':
            return '4TBD'
        elif self.mode == '1DE':
            return '1DE'
        elif self.mode == '1DM':
            return '1DM'
        elif self.mode == '1BD':
            return '1BD'
        elif self.mode == 'TDM':
            return 'TDM'
        else:
            return None

