from queue import Queue
from time import time


class ConsoleQueue:
    def __init__(self, file_name: str, port_list):
        self.command_file = file_name
        self.console_queue = Queue()
        self.ports = port_list

    def start(self):
        while True:
            next_command = self.console_queue.get()
            with open(self.command_file, "a") as command_file:
                command_file.write(next_command)
                command_file.close()

    def add_command(self, command_to_add: str):
        self.console_queue.put_nowait(command_to_add)

    def log_server_status(self, port_list):
        log_server_status_commands = ''
        for port_number in port_list:
            log_server_status_commands += str(port_number) + ",console,logServerStatus" + "\n"
        self.console_queue.put_nowait(log_server_status_commands)

    def announce(self, message: str):
        announce_command = ''
        for port_number in self.ports:
            announce_command += str(port_number) + ",console,serverMessage " + escape(message) + "\n"
        self.add_command(announce_command)

    def change_map(self, port: int, map_name: str):
        self.add_command(str(port) + ",console,changeMap " + escape(map_name) + "\n")

    def start_tournament(self, port: int):
        self.add_command(str(port) + ",console,startTournament" + "\n")

    def stop_tournament(self, port: int):
        self.add_command(str(port) + ",console,stopTournament" + "\n")

    def assign_team(self, port: int, nickname: str, team_number: int):
        if nickname is not None:
            self.add_command(str(port) + ',console,assignTeam "' + escape(nickname) + '" ' + str(team_number) + "\n")

    def modify_tournament(self, port: int, nickname: str, team_number: int):
        if nickname is not None:
            self.add_command(str(port) + ',console,modifyTournament "' + escape(nickname) + '" ' +
                             str(team_number) + "\n")

    def modify_tournament_and_assign(self, port: int, nickname: str, team_number: int):
        if nickname is not None:
            server_command = str(port) + ',console,modifyTournament "' + escape(nickname) + '" ' + \
                             str(team_number) + "\n" + str(port) + ',console,assignTeam "' + \
                             escape(nickname) + '" ' + str(team_number) + "\n"
            self.add_command(server_command)

    def reset_ball(self, port:int, team_num: int):
        self.add_command(str(port) + ",console,resetBallTeam " + str(team_num) + "\n")

    def drop(self, port: int, nickname: str):
        if nickname is not None:
            self.add_command(str(port) + ',console,drop "' + escape(nickname) + '"' + "\n")

    def kick(self, port: int, nickname: str):
        if nickname is not None:
            self.add_command(str(port) + ',console,kick "' + escape(nickname) + '"' + "\n")

    def add_ban(self, id_or_ip: str, duration: int, unit_time: str, reason: str):
        self.add_command(str(list(self.ports)[0]) + ',console,addBan ' + id_or_ip + ' ' + str(duration) + ' ' +
                         unit_time + ' ' + escape(reason) + "\n")

    def add_chat_block(self, id_or_ip: str, duration: int, unit_time: str, reason: str, team_chat: bool):
        self.add_command(str(list(self.ports)[0]) + ',console,addChatBlock ' + id_or_ip + ' AllChat ' +
                         str(duration) + ' ' + unit_time + ' ' + escape(reason) + "\n")
        if team_chat:
            self.add_command(str(list(self.ports)[0]) + ',console,addChatBlock ' + id_or_ip + ' TeamChat ' +
                             str(duration) + ' ' + unit_time + ' ' + escape(reason) + "\n")

    def add_ban_list(self, bans):
        if bans[0] is not None and bans[1] is not None:
            ban_command_list = ''
            current_epoch = int(time())
            for banned_ip in bans[0]:
                ban_command_list += str(list(self.ports)[0]) + ',console,addBan ' + banned_ip[0] + ' 1 ' + \
                    'forever' + ' ' + escape('Severe Rule Violations') + "\n"
            for banned_id in bans[1]:
                if current_epoch < banned_id[2]:
                    ban_minutes = int((banned_id[2] - current_epoch) / 60)
                    if ban_minutes > 599900:  # permanent ban
                        ban_command_list += str(list(self.ports)[0]) + ',console,addBan ' + banned_id[0] + ' 1 ' + \
                            'forever' + ' ' + escape(banned_id[3]) + "\n"
                    elif 1 < ban_minutes <= 9999:
                        ban_command_list += str(list(self.ports)[0]) + ',console,addBan ' + banned_id[0] + ' ' + \
                            str(ban_minutes) + ' ' + 'minute' + ' ' + escape(banned_id[3]) + "\n"
                    elif ban_minutes > 1:
                        ban_hours = int(ban_minutes / 60)
                        ban_command_list += str(list(self.ports)[0]) + ',console,addBan ' + banned_id[0] + ' ' + \
                            str(ban_hours) + ' ' + 'hour' + ' ' + escape(banned_id[3]) + "\n"
            if ban_command_list != '':
                self.add_command(ban_command_list)

    def add_block_list_all(self, blocks):
        if blocks[0] is not None and blocks[1] is not None:
            block_command_list = ''
            current_epoch = int(time())
            for blocked_ip in blocks[0]:
                block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_ip[0] + \
                    ' AllChat 1 forever' + ' ' + escape('Severe Rule Violations') + "\n"
            for blocked_id in blocks[1]:
                if current_epoch < blocked_id[2]:
                    block_minutes = int((blocked_id[2] - current_epoch) / 60)
                    if block_minutes > 599900:  # permanent block
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' AllChat 1 forever' + ' ' + escape(blocked_id[3]) + "\n"
                    elif 1 < block_minutes <= 9999:
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' AllChat ' + str(block_minutes) + ' ' + 'minute' + ' ' + escape(blocked_id[3]) + "\n"
                    elif block_minutes > 1:
                        block_hours = int(block_minutes / 60)
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' AllChat ' + str(block_hours) + ' ' + 'hour' + ' ' + escape(blocked_id[3]) + "\n"
            if block_command_list != '':
                self.add_command(block_command_list)

    def add_block_list_team(self, blocks):
        if blocks[0] is not None and blocks[1] is not None:
            block_command_list = ''
            current_epoch = int(time())
            for blocked_ip in blocks[0]:
                block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_ip[0] + \
                    ' TeamChat 1 forever' + ' ' + escape('Severe Rule Violations') + "\n"
            for blocked_id in blocks[1]:
                if current_epoch < blocked_id[2]:
                    block_minutes = int((blocked_id[2] - current_epoch) / 60)
                    if block_minutes > 599900:  # permanent block
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' TeamChat 1 forever' + ' ' + escape(blocked_id[3]) + "\n"
                    elif 1 < block_minutes <= 9999:
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' TeamChat ' + str(block_minutes) + ' ' + 'minute' + ' ' + escape(blocked_id[3]) + "\n"
                    elif block_minutes > 1:
                        block_hours = int(block_minutes / 60)
                        block_command_list += str(list(self.ports)[0]) + ',console,addChatBlock ' + blocked_id[0] + \
                            ' TeamChat ' + str(block_hours) + ' ' + 'hour' + ' ' + escape(blocked_id[3]) + "\n"
            if block_command_list != '':
                self.add_command(block_command_list)

    def remove_ban(self, id_or_ip: str):
        self.add_command(str(list(self.ports)[0]) + ',console,removeBan ' + id_or_ip + "\n")

    def remove_chat_block(self, id_or_ip: str, team_chat: bool):
        self.add_command(str(list(self.ports)[0]) + ',console,removeChatBlock ' + id_or_ip + " AllChat\n")
        if team_chat:
            self.add_command(str(list(self.ports)[0]) + ',console,removeChatBlock ' + id_or_ip + " TeamChat\n")

    def clear_bans(self, ban_list):
        if len(ban_list) > 0:
            ban_command = ''
            for ban_item in ban_list:
                ban_command += str(list(self.ports)[0]) + ',console,removeBan ' + ban_item + "\n"
            self.add_command(ban_command)

    def clear_chat_blocks_all(self, block_list):
        if len(block_list) > 0:
            block_command = ''
            for block_item in block_list:
                block_command += str(list(self.ports)[0]) + ',console,removeChatBlock ' + block_item + " AllChat\n"
            self.add_command(block_command)

    def clear_chat_blocks_team(self, block_list):
        if len(block_list) > 0:
            block_command = ''
            for block_item in block_list:
                block_command += str(list(self.ports)[0]) + ',console,removeChatBlock ' + block_item + " TeamChat\n"
            self.add_command(block_command)

    def server_message(self, port: int, message: str):
        self.add_command(str(port) + ",console,serverMessage " + escape(message) + "\n")

    def server_message_multi(self, port: int, message_list):
        if len(message_list) > 0:
            command_text = ''
            for message in message_list:
                command_text += str(port) + ",console,serverMessage " + escape(message) + "\n"
            self.add_command(command_text)

    def set_plane_scale(self, port: int, plane_scale: int):
        self.add_command(str(port) + ",console,testPlaneScale " + str(plane_scale) + "\n")

    def modify_spec_chat(self, port: int, enabled: bool):
        self.add_command(str(port) + ",console,allowSpectatorAllChat " + str(enabled).lower() + "\n")

    def server_whisper(self, port: int, nickname: str, message: str):
        if nickname is not None:
            self.add_command(str(port) + ',console,serverWhisper "' + escape(nickname) + '" ' + escape(message) + "\n")

    def server_whisper_list(self, port: int, nickname_list, message: str):
        if len(nickname_list) > 0:
            command_text = ''
            for nickname in nickname_list:
                command_text += str(port) + ',console,serverWhisper "' + escape(nickname) + '" ' + escape(message) + \
                    "\n"
            self.add_command(command_text)

    def server_whisper_list_multi(self, port: int, nickname_list, message_list):
        if len(nickname_list) > 0:
            command_text = ''
            for nickname in nickname_list:
                for message in message_list:
                    if message != '':
                        command_text += str(port) + ',console,serverWhisper "' + escape(nickname) + '" ' + \
                            escape(message) + "\n"
            self.add_command(command_text)

    def stop_assign_and_start(self, port: int, assignments):
        server_command = str(port) + ",console,stopTournament" + "\n"
        for nickname, team_number in assignments.items():
            server_command += str(port) + ',console,assignTeam "' + escape(nickname) + '" ' + str(team_number) + "\n"
        server_command += str(port) + ",console,startTournament" + "\n"
        self.add_command(server_command)

    def assign_and_start(self, port: int, assignments):
        server_command = ''
        for nickname, team_number in assignments.items():
            server_command += str(port) + ',console,assignTeam "' + escape(nickname) + '" ' + str(team_number) + "\n"
        server_command += str(port) + ",console,startTournament" + "\n"
        self.add_command(server_command)

    def assign_list(self, port: int, assignments):
        server_command = ''
        for nickname, team_number in assignments.items():
            server_command += str(port) + ',console,assignTeam "' + escape(nickname) + '" ' + str(team_number) + "\n"
        self.add_command(server_command)

    def server_move(self, current_port: int, ip_address: str, port: int, server_password: str, nickname_list):
        if len(nickname_list) > 0:
            server_command = ''
            for nickname in nickname_list:
                server_command += str(current_port) + ',console,serverRequestPlayerChangeServer "' + \
                    escape(nickname) + '" ' + ip_address + ':' + str(port) + ' ' + server_password + "\n"
            self.add_command(server_command)


def escape(text: str):
    return text.replace('\\', '\\\\').replace('"', '\\"')
