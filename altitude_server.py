from altitude_launcher_config import *
from altitude_player import Player
from altitude_console import ConsoleQueue
from planeball_database import DatabaseQueue


class AltitudeServer:
    def __init__(self, launcher_config: LauncherConfig, is_ladder: bool, short_name: str, maps_by_mode,
                 console: ConsoleQueue, database: DatabaseQueue, events, server_log_queue, seasons, lobby, region: str,
                 modes_per_rating_group, maps_by_rating_group, server_type: str, server_ip: str,
                 server_id: str, hide_column: str):
        self.port = launcher_config.port
        self.is_ladder = is_ladder
        self.launcher_config = launcher_config
        self.short_name = short_name
        self.modes_per_rating_group = modes_per_rating_group
        """ :type: dict [str, str] """
        self.maps_by_rating_group = maps_by_rating_group
        """ :type: dict [str, str] """
        self.server_groups = list()
        self.maps_by_mode = maps_by_mode
        self.map_name = launcher_config.map_rotation_list[0]
        self.console = console
        """ :type: ConsoleQueue """
        self.database = database
        """ :type: DatabaseQueue """
        self.initialized = False
        self.server_type = server_type
        self.server_id = server_id
        self.server_ip = server_ip
        self.server_log_queue = server_log_queue
        """ :type: Queue """
        self.map_mode = launcher_config.map_list[launcher_config.map_rotation_list[0]].mode
        self.region = region
        self.seasons = seasons
        """ :type: dict """
        self.teams = dict()
        """ :type: dict [int, list [int]] """
        self.teams[-1] = list()
        self.teams[0] = list()
        self.teams[1] = list()
        self.teams_bots = dict()
        """ :type: dict [int, list [int]] """
        self.teams[-1] = []
        self.teams_bots[-1] = []
        self.teams_assigned = dict()
        """ :type: dict [int, list [str]] """
        self.teams_assigned[0] = list()
        self.teams_assigned[1] = list()
        self.teams_expected = dict()
        """ :type: dict [int, list [str]] """
        self.teams_expected[0] = list()
        self.teams_expected[1] = list()
        self.active_teams = [2]
        self.players = dict()
        """ :type: dict [str, Player] """
        # AltitudeGame class handles most of the work with disconnected players.
        # However, AltitudeGame only saves players for the current game (lost after map change).
        # disconnected_tournament_players saves info on players who disconnect with tournament started in lobby
        self.disconnected_tournament_players = dict()
        """ :type: dict [str, Player] """
        self.players_by_number = dict()
        """ :type: dict [int, str] """
        self.players_by_number_disconnected = dict()
        """ :type: dict [int, str] """
        self.pings = dict()
        """ :type: dict [str, OrderedDict [int, int]] """
        self.tournament = False
        self.spawn_timer = dict()
        self.game_timer = 0
        self.left_team_id = 2
        self.right_team_id = 2
        self.lagging = dict()
        """ :type: dict [str, int] """
        self.starting_tournament = False
        self.stopping_tournament = False
        self.changing_map = False
        self.remove_from_tournament_list = list()
        """ :type: list [str] """
        self.join_queue = list()
        """ :type: list [str] """
        self.game = None
        """ :type: AltitudeGame """
        self.game_previous = None
        """ :type: AltitudeGame """
        self.voting = dict()  # None or event_id of vote
        """ :type: dict [str, int] """
        self.voting['all'] = None
        self.voting['specs'] = None
        self.voting['playing'] = None
        self.events = events
        self.status = 'Idle'  # Idle, Ranked, Unranked
        """ :type: str """
        self.ladder_maps = dict()
        self.league_maps = dict()
        self.ladder_maps_playable = dict()
        self.repeat_map_list = None
        self.recent_maps = list()
        self.last_spec = list()
        self.lobby = lobby
        self.next_map = None
        self.start_attempts = 0
        self.last_map_change_time = None
        self.restarting = False
        self.hide_column = hide_column

    def client_add(self, player_number: int, vapor_id: str, nickname: str, ip: str, level: int, ace_rank: int,
                   log_time: int, silent: bool):
        pass
