from collections import OrderedDict


class TournamentData:
    def __init__(self):
        self.game_stats = PlayerGameStats()
        self.spawn_stats_recent = OrderedDict()
        """ :type: OrderedDict [int, PlaneStats] """
        self.spawn_stats_active = None
        """ :type: PlaneStats """
        self.plane_stats = list()
        """ :type: list [PlaneStats] """


class ModeSeasonStats:
    def __init__(self):
        self.wins = None
        self.losses = None
        self.ties = None
        self.damage_dealt = None
        self.damage_received = None
        self.win_percentage = None
        self.kill_streak = None
        self.longest_life = None
        self.plus_minus = None
        self.per_game_goals = None
        self.per_game_goals_assisted = None
        self.per_game_own_goals = None
        self.per_game_base_damage = None
        self.goals = None
        self.goals_assisted = None
        self.points = None  # goals + assists unless mode = 4BALL, then number of points scored
        self.own_goals = None
        self.base_damage = None
        self.time_played = None
        self.kills = None
        self.ball_kills = None
        self.deaths = None
        self.passes = None
        self.catches = None
        self.turnovers = None
        self.ball_time = None
        self.ratio_catches_to_turnovers = None
        self.ratio_kills_to_deaths = None
        self.per_ten_minutes_ball_time = None
        self.per_ten_minutes_kills = None
        self.per_ten_minutes_ball_kills = None
        self.per_ten_minutes_passes = None


class RatingGroupSeasonStats:
    def __init__(self):
        self.rating = None
        self.win_streak = None
        self.loss_streak = None
        self.current_streak = None
        self.recent_games = None
        self.clutch_points = None
        self.promotion = None


class PlayerGameStats:
    def __init__(self):
        self.team = None
        self.kill_streak = 0
        self.longest_life = 0
        self.time_since_last_death = 0
        self.clutch_points = 0
        self.lives_left = 1
        self.plus_minus = 0
        self.goals = 0
        self.goals_assisted = 0
        self.points = 0   # number of points scored (used by 4BALL)
        self.own_goals = 0
        self.base_damage = 0
        self.turret_damage = 0
        self.turrets_destroyed = 0
        self.damage_dealt = 0.0
        self.damage_received = 0.0
        self.time_start_ms = 0
        self.time_end_ms = None
        self.time_start_ms_2 = None
        self.time_end_ms_2 = None
        self.last_charge_pickup_time = None
        self.last_charge_drop_time = None
        self.last_bomb_drop_team = None
        self.last_bomb_pickup_time = None
        self.last_bomb_drop_time = None
        self.last_ball_drop_team = None
        self.last_ball_pickup_time = None
        self.last_ball_drop_time = None
        self.last_ball_drop_was_pass = False
        # Times below used to make sure players are spawning
        self.expected_spawn_time = None
        self.joined_team_time = None


class PlaneStats:
    def __init__(self, vapor_id: str, red_perk: str, green_perk: str, blue_perk: str, team_number: int):
        self.vapor_id = vapor_id
        self.red_perk = red_perk
        self.green_perk = green_perk
        self.blue_perk = blue_perk
        self.team_number = team_number
        self.spawns = 1
        self.spawn_start = None
        self.spawn_end = None
        self.time_used = 0
        self.kills = 0
        self.assists = 0
        self.turret_damage = 0
        self.turrets_destroyed = 0
        self.deaths = 0
        self.crashes = 0
        self.multikill = 0
        self.plus_minus = 0
        # BALL
        self.ball_time = 0
        self.goals = 0
        self.goals_assisted = 0
        self.points = 0   # number of points scored (used by 4BALL)
        self.own_goals = 0
        self.catches = 0
        self.passes = 0
        self.steals = 0
        self.turnovers = 0
        self.ball_kills = 0
        # TBD
        self.bomb_time = 0
        self.bomb_defuses = 0
        self.bases_destroyed = 0
        self.base_damage = 0
        self.bomb_kills = 0
        # 1DE
        self.charge_time = 0
        self.charge_plants = 0
        self.charge_defuses = 0
        self.charge_kills = 0
        # Dodgeball
        self.dodgeball_hits = 0
        self.dodgeball_outs = 0

    def __str__(self):
        return 'vapor_id: ' + str(self.vapor_id) + ', red_perk: ' + str(self.red_perk) + ', green_perk: ' + \
               str(self.green_perk) + ', blue_perk: ' + str(self.blue_perk) + ', spawns: ' + str(self.spawns) + \
               ', time_used: ' + str(self.time_used) + ', kills: ' + str(self.kills) + ', charge_kills: ' + \
               str(self.charge_kills) + ', ball_kills: ' + str(self.ball_kills) + \
               ', bomb_kills: ' + str(self.bomb_kills) + \
               ', assists: ' + str(self.assists) + ', base_damage: ' + str(self.base_damage) + ', turret_damage: ' + \
               str(self.turret_damage) + ', bases_destroyed: ' + str(self.bases_destroyed) + ', turrets_destroyed: ' + \
               str(self.turrets_destroyed) + ', goals: ' + str(self.goals) + ', goals_assisted: ' + \
               str(self.goals_assisted) +  ', points: ' + str(self.points) + ', own goals: ' + str(self.own_goals) + \
               ', catches: ' + str(self.catches) + ', passes: ' + \
               str(self.passes) + ', steals: ' + str(self.steals) + \
               ', turnovers: ' + str(self.turnovers) + ', charge_plants: ' + str(self.charge_plants) + \
               ', charge_defuses: ' + str(self.charge_defuses) + ', bomb_defuses: ' + str(self.bomb_defuses) + \
               ', deaths: ' + str(self.deaths) + ', crashes: ' + str(self.crashes) + ', ball_time: ' + \
               str(self.ball_time) + ', bomb_time: ' + str(self.bomb_time) + ', charge_time: ' + \
               str(self.charge_time) + ', multikill: ' + str(self.multikill) + ', dodgeball_hits: ' + \
               str(self.dodgeball_hits) + ', dodgeball_outs: ' + str(self.dodgeball_outs) + \
               ', plus_minus: ' + str(self.plus_minus)
