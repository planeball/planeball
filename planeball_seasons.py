from collections import OrderedDict
from planeball_database import DatabaseQueue


class Season:
    def __init__(self, rating_group: str, season_id: int, season_number: int, league: str, tiers_col: str):
        self.rating_group = rating_group
        self.id = season_id
        self.number = season_number
        self.league = league
        self.tiers_col = tiers_col


def get_seasons(db: DatabaseQueue, league: str):
    ranked_modes = OrderedDict()
    query = ("SELECT rating_group, season, season_number, tiers_col FROM seasons "
             "WHERE league=? AND start_date <= CURDATE() AND end_date >= CURDATE() ORDER BY rating_group")
    parameters = (league, )
    results = db.select(query, parameters)
    if results is not None:
        for (current_rating_group, current_season_id, current_season_number, current_tiers_col) in results:
            ranked_modes[current_rating_group] = \
                Season(current_rating_group, current_season_id, current_season_number, 'Ranked', current_tiers_col)
    return ranked_modes
