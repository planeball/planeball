import mariadb
import time
from queue import Queue
from threading import Thread
import logging


class DatabaseQueue:
    def __init__(self, connection, log_file: str, log_level: int):
        self.connection = connection
        self.query_queue = Queue()
        self.return_data = dict()
        self.query_id = 0
        self.close_connection = False
        self.connection_closed = False
        _database_thread = Thread(target=self.database_connection, args=())
        _database_thread.start()
        logging.info('Database Thread Started')

    def database_connection(self):
        critical_error = False
        try:
            cnx = mariadb.connect(**self.connection)
            logging.info('mariadb connection opened')
            while not self.close_connection:
                next_request = self.query_queue.get()
                cursor = None
                query = None
                parameters = None
                try:
                    if next_request == 'CLOSE':
                        self.close_connection = True
                    else:
                        data_result = []
                        if next_request['commit'] is False:
                            cursor = cnx.cursor(buffered=next_request['buffered'])
                            query = str(next_request['query'])
                            parameters = str(next_request['parameters'])
                            cursor.execute(next_request['query'], next_request['parameters'])
                            for data_row in cursor:
                                data_result.append(data_row)
                            self.return_data[next_request['id']] = data_result
                        else:
                            cursor = cnx.cursor()
                            for query_tuple in next_request['query_list']:
                                query = str(query_tuple[0])
                                parameters = str(query_tuple[1])
                                cursor.execute(query_tuple[0], query_tuple[1])
                            self.return_data[next_request['id']] = True
                        cnx.commit()
                        cursor.close()
                except mariadb.Error as err:
                    cnx.rollback()
                    if cursor is not None:
                        cursor.close()
                    logging.exception(err)
                    logging.error('QUERY: ' + query)
                    logging.error('PARAMS: ' + parameters)
                    self.return_data[next_request['id']] = None
        except mariadb.Error as err:
            critical_error = True
            logging.exception(err)
        else:
            cnx.close()
            self.connection_closed = True
        finally:
            if critical_error:
                raise Exception('mysql.connector.Error on Initialize')

    def select(self, query: str, parameters, buffered=True):
        query_data = {'id': self.query_id, 'commit': False, 'query': query, 'parameters': parameters,
                      'buffered': buffered}
        logging.debug('Database Select: ' + str(query_data))
        self.return_data[query_data['id']] = None
        self.query_id += 1
        self.query_queue.put_nowait(query_data)
        while self.return_data[query_data['id']] is None and self.close_connection is False:
            time.sleep(0.1)
        if self.close_connection:
            return None
        else:
            return self.return_data.pop(query_data['id'])

    def commit(self, query_list):
        query_data = {'id': self.query_id, 'commit': True, 'query_list': query_list}
        logging.debug('Database Commit: ' + str(query_data))
        self.return_data[query_data['id']] = False
        self.query_id += 1
        self.query_queue.put_nowait(query_data)
        while self.return_data[query_data['id']] is not None and self.return_data[query_data['id']] is False:
            time.sleep(0.1)
        if self.return_data[query_data['id']] is None:
            self.return_data[query_data['id']] = False
        return self.return_data.pop(query_data['id'])

    def close(self):
        self.query_queue.put_nowait('CLOSE')
