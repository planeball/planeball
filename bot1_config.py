from xml.etree import ElementTree
from datetime import datetime
import logging


class DiscordConfig:
    def __init__(self, file: str):
        self.log_level = logging.INFO
        self.log_file = 'bot1'
        self.token = None
        self.id_guild = None
        self.id_ranked_status = None
        self.id_unranked_status = None
        self.id_ranked_games = None
        self.id_unranked_games = None
        self.id_ranked_starting = None
        self.id_unranked_starting = None
        self.id_general = None
        self.id_admin_info = None
        self.id_ranked_4v4_ratings = None
        self.id_ranked_4v4_points = None
        self.id_ranked_4v4_assists = None
        self.id_ranked_4v4_kills = None
        self.id_ranked_4v4_win_streak = None
        self.id_ranked_4v4_loss_streak = None
        self.read_xml_config(file)
        self.start_time = datetime.now()
        self.log_file = 'logs/' + self.log_file + '_' + str(self.start_time.year) + "-" + \
                        str(self.start_time.month).zfill(2) + "-" + str(self.start_time.day).zfill(2) + "__" + \
                        str(self.start_time.hour).zfill(2) + "-" + str(self.start_time.minute).zfill(2) + '.log'
        logging.basicConfig(filename=self.log_file, filemode='w', level=self.log_level,
                            format='%(asctime)s [%(levelname)s] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        if self.token is None:
            logging.exception('Discord bot token not defined in Bot1 Config XML File (' + file + ')')
            raise Exception('Discord bot token not defined in Bot1 Config XML File (' + file + ')')
        else:
            logging.info('Bot1 config loaded successfully')

    def read_xml_config(self, file: str):
        tree = ElementTree.parse(file)
        root = tree.getroot()
        if 'log_level' in root.attrib:
            if root.attrib['log_level'].upper() == 'DEBUG':
                self.log_level = logging.DEBUG
            elif root.attrib['log_level'].upper() == 'INFO':
                self.log_level = logging.INFO
            elif root.attrib['log_level'].upper() == 'WARNING':
                self.log_level = logging.WARNING
            elif root.attrib['log_level'].upper() == 'ERROR':
                self.log_level = logging.ERROR
            elif root.attrib['log_level'].upper() == 'CRITICAL':
                self.log_level = logging.CRITICAL
        if 'log_file' in root.attrib:
            self.log_file = root.attrib['log_file']
        if 'token' in root.attrib:
            self.token = root.attrib['token']
        for child in root:
            if child.tag.upper() == "IDS":
                for child_id in child:
                    if child_id.tag.upper() == 'GUILD':
                        self.id_guild = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_STATUS':
                        self.id_ranked_status = child_id.attrib['value']
                    elif child_id.tag.upper() == 'UNRANKED_STATUS':
                        self.id_unranked_status = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_GAMES':
                        self.id_ranked_games = child_id.attrib['value']
                    elif child_id.tag.upper() == 'UNRANKED_GAMES':
                        self.id_unranked_games = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_STARTING':
                        self.id_ranked_starting = child_id.attrib['value']
                    elif child_id.tag.upper() == 'UNRANKED_STARTING':
                        self.id_unranked_starting = child_id.attrib['value']
                    elif child_id.tag.upper() == 'GENERAL':
                        self.id_general = child_id.attrib['value']
                    elif child_id.tag.upper() == 'ADMIN_INFO':
                        self.id_admin_info = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_RATINGS':
                        self.id_ranked_4v4_ratings = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_POINTS':
                        self.id_ranked_4v4_points = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_ASSISTS':
                        self.id_ranked_4v4_assists = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_KILLS':
                        self.id_ranked_4v4_kills = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_WIN_STREAK':
                        self.id_ranked_4v4_win_streak = child_id.attrib['value']
                    elif child_id.tag.upper() == 'RANKED_4v4_LOSS_STREAK':
                        self.id_ranked_4v4_loss_streak = child_id.attrib['value']
