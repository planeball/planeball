from threading import Thread
from altitude_test_log import LogWriter, LogPlayer
from altitude_files import AltitudeFiles
from altitude_console import ConsoleQueue
from altitude_config import AltitudeConfig
from altitude_server_config import read_launcher_config

player_1 = LogPlayer('00000001-0000-0000-0000-000000000000', 'P1', 60, 5, '192.168.0.1')
player_2 = LogPlayer('00000002-0000-0000-0000-000000000000', 'P2', 56, 1, '192.168.0.2')
player_3 = LogPlayer('00000003-0000-0000-0000-000000000000', 'P3', 10, 2, '192.168.0.3')
player_4 = LogPlayer('00000004-0000-0000-0000-000000000000', 'P4', 5, 1, '192.168.0.4')
player_5 = LogPlayer('00000005-0000-0000-0000-000000000000', 'P5', 60, 7, '192.168.0.5')

altitude_config = AltitudeConfig('altitude_config.xml')
files = AltitudeFiles(altitude_config.launcher_config)
file_check = files.files_exist()
server_config = read_launcher_config(files.launcher_config)
""" :type: OrderedDict [int, ServerConfig] """

console = ConsoleQueue(files.command, server_config.keys())
log = LogWriter(files.server_log, server_config.keys())


def script1():
    port1 = list(server_config)[0]
    log.client_add(port1, 32445, player_1)
    log.team_change(port1, 32448, player_1, 3)


if __name__ == '__main__':
    script1()
