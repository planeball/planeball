import logging
from planeball_database import DatabaseQueue
from planeball_seasons import Season
from prettytable import PrettyTable


class PlayerBasicInfo:
    def __init__(self, player_id: int, aka: str, nickname: str):
        self.id = player_id
        self.aka = aka
        self.nickname = nickname


def get_player_basics_from_id(id_player: int, db: DatabaseQueue):
    selected_player = None
    query = 'SELECT aka, nickname FROM players WHERE id_player=?'
    parameters = (id_player,)
    results = db.select(query, parameters)
    if results is not None:
        if len(results) == 1:
            selected_player = PlayerBasicInfo(id_player, results[0][0], results[0][1])
    return selected_player


def get_player_basics_from_aka(aka: str, db: DatabaseQueue):
    selected_player = None
    query = ("SELECT players.aka, nickname, players.id_player FROM players "
             "LEFT JOIN akas ON players.id_player = akas.id_player WHERE akas.aka=?")
    parameters = (aka,)
    results = db.select(query, parameters)
    if results is not None:
        if len(results) == 1:
            selected_player = PlayerBasicInfo(results[0][2], results[0][0], results[0][1])
    return selected_player


def generate_player_info(selected_player: PlayerBasicInfo, db:DatabaseQueue, seasons: dict):
    rank_table = PrettyTable()
    rank_table.field_names = ['Mode', 'Tier', '#', 'Rating']
    if selected_player is not None:
        for ranked_mode in seasons.values():
            group_tier = None
            group_tier_string = None
            query = 'SELECT ' + ranked_mode.tiers_col + ' FROM players WHERE id_player=%s'
            tier_data = db.select(query, (selected_player.id, ))
            if tier_data is not None:
                group_tier = tier_data[0][0]
                if group_tier == 0:
                    group_tier_string = 'DIAMOND'
                elif group_tier == 1:
                    group_tier_string = 'GOLD'
                elif group_tier == 2:
                    group_tier_string = 'SILVER'
                elif group_tier == 3:
                    group_tier_string = 'BRONZE'
            mode_query = ("SELECT player_rank, rating, current_streak FROM "
                          "(SELECT RANK() OVER (ORDER BY rating DESC) player_rank, rating, nickname, current_streak, "
                          "players.id_player FROM players "
                          "LEFT JOIN group_stats ON players.id_player=group_stats.id_player "
                          "WHERE rating_group=? AND league=? AND season=? and tier_ball_4v4_s11=? ) "
                          "AS temp2 WHERE id_player=?;")
            mode_result = db.select(mode_query, (ranked_mode.rating_group, 'Ranked', ranked_mode.id,
                                                 group_tier, selected_player.id), buffered=True)
            if mode_result is not None and len(mode_result) > 0:
                rank_table.add_row(
                    [ranked_mode.rating_group, group_tier_string, str(mode_result[0][0]), str(mode_result[0][1])])
        player_info = 'Nickname: ' + selected_player.nickname
        if selected_player.aka is not None:
            player_info += ' (@' + selected_player.aka + ')'
        if len(rank_table.rows) > 0:
            player_info += '\n`' + rank_table.get_string() + '`'
    else:
        player_info = None
    return player_info


def slash_id(id_player: int, db: DatabaseQueue, seasons: dict):
    player_info = generate_player_info(get_player_basics_from_id(id_player, db), db, seasons)
    if player_info is None:
        return 'No player exists with that id.'
    else:
        return player_info


def slash_aka(aka: str, db: DatabaseQueue, seasons: dict):
    player_info = generate_player_info(get_player_basics_from_aka(aka, db), db, seasons)
    if player_info is None:
        return 'That aka has not been assigned to any player.'
    else:
        return player_info
