import logging
from datetime import datetime
import os
import xml.etree.ElementTree


def required_files_exist(file_list):
    for file_name in file_list:
        if not os.path.exists(file_name):
            return False
    return True


def get_bans_from_file(file_name: str):
    tree = xml.etree.ElementTree.parse(file_name)
    root = tree.getroot()
    bans_to_remove = []
    for child in root:
        if child.tag.upper() == 'BANNEDIPS':
            for sub_child in child:
                bans_to_remove.append(sub_child.attrib['ip'])
        if child.tag.upper() == 'BANNEDVAPORIDS':
            for sub_child in child:
                bans_to_remove.append(sub_child.attrib['vaporId'])
    return bans_to_remove


class AltitudeConfig:
    def __init__(self, file: str):
        self.log_level = logging.INFO
        self.log_file = 'altitude'
        self.name = None
        self.region = None
        self.ip = None
        self.id_server = 1
        self.launcher_config = 'launcher_config'
        self.read_xml_config(file)
        self.start_time = datetime.now()
        self.log_file = 'logs/' + self.log_file + '_' + str(self.start_time.year) + "-" + \
                        str(self.start_time.month).zfill(2) + "-" + str(self.start_time.day).zfill(2) + "__" + \
                        str(self.start_time.hour).zfill(2) + "-" + str(self.start_time.minute).zfill(2) + '.log'
        logging.basicConfig(filename=self.log_file, filemode='w', level=self.log_level,
                            format='%(asctime)s [%(levelname)s] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        if self.name is None:
            logging.exception('Server name not defined in Altitude Config XML File (' + file + ')')
            raise Exception('Server name not defined in Altitude Config XML File (' + file + ')')
        elif self.region is None:
            logging.exception('Server region not defined in Altitude Config XML File (' + file + ')')
            raise Exception('Server region not defined in Altitude Config XML File (' + file + ')')
        elif self.ip is None:
            logging.exception('Server ip not defined in Altitude Config XML File (' + file + ')')
            raise Exception('Server ip not defined in Altitude Config XML File (' + file + ')')
        else:
            logging.info('Altitude config loaded successfully')

    def read_xml_config(self, file_name: str):
        tree = xml.etree.ElementTree.parse(file_name)
        root = tree.getroot()
        if 'log_level' in root.attrib:
            if root.attrib['log_level'].upper() == 'DEBUG':
                self.log_level = logging.DEBUG
            elif root.attrib['log_level'].upper() == 'INFO':
                self.log_level = logging.INFO
            elif root.attrib['log_level'].upper() == 'WARNING':
                self.log_level = logging.WARNING
            elif root.attrib['log_level'].upper() == 'ERROR':
                self.log_level = logging.ERROR
            elif root.attrib['log_level'].upper() == 'CRITICAL':
                self.log_level = logging.CRITICAL
        if 'log_level' in root.attrib:
            self.log_level = root.attrib['log_level']
        if 'name' in root.attrib:
            self.name = root.attrib['name']
        if 'region' in root.attrib:
            self.region = root.attrib['region'].upper()
        if 'ip' in root.attrib:
            self.ip = root.attrib['ip'].upper()
        if 'id_server' in root.attrib:
            self.id_server = root.attrib['id_server'].upper()
        for child in root:
            if child.tag.upper() == "AltitudeFiles":
                for child_id in child:
                    if child_id.tag.upper() == 'launcher_config':
                        self.launcher_config = child_id.attrib['value']
